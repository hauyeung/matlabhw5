function v = sort3(a,b,c)
if (a < b && a < c)
    v(1) = a;
end
if (a > b && a < c)
    v(2) = a;
end
if (a > b && a > c)
    v(3) = a;
end
if (a == b && a < c)
    v(1) = a;
    v(2) = b;
    v(3) = c;
end
if (a == b && a > c)
    v(1) = a;
    v(2) = b;
    v(3) = c;
end
if (b < a && b < c)
    v(1) = b;
end
if (b > a && b < c)
    v(2) = b;
end
if (b > a && b > c)
    v(3) = b;
end
if (a == c && a < b)
    v(1) = a;
    v(2) = c
    v(3) = b;
end
if (a == c && a > b)
    v(1) = b;
    v(2) = a;
    v(3) = c;
end
if (c < a && c < b)
    v(1) = c;
end
if (c > a && c < b)
    v(2) = c;
end
if (c > a && c > b)
    v(3) = c;
end
if (b == c && a < b)
    v(1) = a;
    v(2) = b;
    v(3) = c;
end
if (b == c && a > b)
    v(1) = c;
    v(2) = b;
    v(3) = a;
end
if (a ==b && a ==c) 
    v(1) = a;    
    v(2) = a;
    v(3) = a;
end
if (a > b && b > c)
    v(1) = c;
    v(2) = b;
    v(3) = a;
end
if (a < b && b< c)
    v(1) = a;
    v(2) = b;
    v(3) = c;
end
if (a > b && b < c && a > c)
    v(1) = b;
    v(2) = c;
    v(3) = a;
end
if (a < b && b > c && a > c)
    v(1) =c;
    v(2) = a;
    v(3) = b;
end