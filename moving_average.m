function a = moving_average(x)
persistent nums;
if isempty(nums)
    nums = [];
end
if length(nums) <= 24
    nums = [nums x];
else
    nums = [nums(2:end) x];
end
a = mean(nums);
end