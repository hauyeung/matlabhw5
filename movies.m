function binge = movies( hr1, hr2, min1, min2, durmin1, durmin2)
t1 = datetime(2000,1,1,hr1,min1,0);
t2 = datetime(2000,1,1,hr2,min2,0);
endt1 = t1 + minutes(durmin1);
if (endt1 < t2)
    if (minutes(t2 - endt1) <= 30 && minutes(t2 - endt1) > 0)
        binge = true;
    elseif (minutes(t2 - endt1) > 30)
        binge = true;
    end
elseif (endt1 >= t2)
    binge = false;
end
end